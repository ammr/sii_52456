#include "DatosMemCompartida.h"
#include <sys/stat.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/mman.h>

DatosMemCompartida *datosmemcompartida;
int main(int argc, char **argv){
	
	int fdmem = open ("/tmp/bot.txt", O_RDWR, 0777);
	datosmemcompartida=(DatosMemCompartida*)mmap(NULL, sizeof(DatosMemCompartida), PROT_WRITE|PROT_READ, MAP_SHARED, fdmem,0);
	close(fdmem);
	
	while(1){
		if(datosmemcompartida->esfera.centro.y > datosmemcompartida->raqueta1.y2)
			datosmemcompartida->accion=1;
		else
			if(datosmemcompartida->esfera.centro.y < datosmemcompartida->raqueta1.y1)
				datosmemcompartida->accion=-1;
			else
				datosmemcompartida->accion=0;
		usleep(25000);
	}
return 0;
}
