//Recibe una cadena de texto del juego cada vez que uno de los dos jugadores anota un punto. La comunicacion entre logger y tenis se debe realizar mediante una tuberia con nombre.
//crear mkfifo, lectura open, destruirla close y unlink
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "glut.h"
#define MAX 100



int main(int argc, char *argv[]){
	
	int fd_loggermundo = open("/tmp/tuberia_logger",O_RDONLY);
	char mensaje[MAX];
	mkfifo("/tmp/tuberia_logger",0777);

	while(1)
	{
	read(fd_loggermundo, mensaje, sizeof(mensaje));
	printf("%s\n",mensaje);
	if(mensaje[0]=='c')
		break;
	}
	close(fd_loggermundo);
	unlink("/tmp/tuberia_logger");
	return 1;
}
